package com.cice.derechupeteapp.extensions

import android.util.Log
import androidx.databinding.library.BuildConfig

//Log Method
fun Any?.log(tag: String = "MOMO TAG") {
    if (BuildConfig.DEBUG)
        Log.d(tag, this.toString())
}