package com.cice.derechupeteapp.usescase.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cice.derechupeteapp.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}