package com.cice.derechupeteapp.usescase.start

import android.os.Bundle
import com.cice.derechupeteapp.R
import com.cice.derechupeteapp.globals.GlobalActivity

class StartActivity : GlobalActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start_activity)
    }
}